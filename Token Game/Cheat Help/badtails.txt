Without any cheats in the game, if you flip the coin
and get tails, you will still get points, but not from
your opponent. This cheat makes it so you lose your turn
if you get tails.