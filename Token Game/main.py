import tkinter as tk
from os import system
from random import choice
from tkinter import messagebox as osmessage
from tkinter import simpledialog
root = tk.Tk()
states = ['disabled', 'active']
setting = osmessage.askyesno(
    '', 'Would you like to play in Tutorial Mode?')
if setting == True:
    system('open howtoplay.txt')
win = tk.Tk()
root.title('Token Game')
root.geometry('400x250')
root.resizable(False, False)

global index, status
option1 = 'You get to take tokens from the infinite pool of tokens'
option2 = 'You get to take tokens from the other team'
index = True
status = None
Teams = [simpledialog.askstring(
    "Player 1's name", 'Name:'), simpledialog.askstring("Player 2's name", 'Name:')]
DiceSides = [1, 2, 3, 4, 5, 6]
CoinSides = ['Heads', 'Tails']
win.title(Teams[0])
winner = 20
loser = 0

# Red Side Variables and Functions \/
RedSide = tk.LabelFrame(root, text=Teams[1] + "'s Team/Side")
RedSide.place(x=0, y=0)
RedPoints = tk.StringVar()
RedPoints.set('7')
RedCount = tk.Label(RedSide, font=('Helvetica', 16),
                    textvariable=RedPoints).pack()
# Red Side Variables and Functions /\

# Blue Side Variables and Functions \/
BlueSide = tk.LabelFrame(root, text=Teams[0] + "'s Team/Side")
BlueSide.place(x=0, y=50)
BluePoints = tk.StringVar()
BluePoints.set('7')
BlueCount = tk.Label(
    BlueSide, font=('Helvetica', 16), textvariable=BluePoints).pack()
# Blue Side Variables and Functions /\


def switch(cheats):
    global winner
    if cheats == 'InfiFlip':
        outvar = not InfiFlip.get()
        InfiFlip.set(outvar)
        states[0] = tk.ACTIVE
    if cheats == 'BadTails':
        outvar = not BadTails.get()
        BadTails.set(outvar)
    if cheats == 'Sides10':
        BluePoints.set('11')
        RedPoints.set('11')
        winner = 30
        DiceSides.append(7)
        DiceSides.append(8)
        DiceSides.append(9)
        DiceSides.append(10)
    if cheats == 'Sides12':
        BluePoints.set('13')
        RedPoints.set('13')
        winner = 35
        DiceSides.append(7)
        DiceSides.append(8)
        DiceSides.append(9)
        DiceSides.append(10)
        DiceSides.append(11)
        DiceSides.append(12)
    if cheats == 'Sides20':
        BluePoints.set('21')
        RedPoints.set('21')
        winner = 40
        DiceSides.append(7)
        DiceSides.append(8)
        DiceSides.append(9)
        DiceSides.append(10)
        DiceSides.append(11)
        DiceSides.append(12)
        DiceSides.append(13)
        DiceSides.append(14)
        DiceSides.append(15)
        DiceSides.append(16)
        DiceSides.append(17)
        DiceSides.append(18)
        DiceSides.append(19)
        DiceSides.append(20)
    if cheats == 'All6':
        DiceSides.remove(1)
        DiceSides.remove(2)
        DiceSides.remove(3)
        DiceSides.remove(4)
        DiceSides.remove(5)


def CheatCode():
    code = simpledialog.askstring('Cheat Code', 'Enter a Cheat Code')
    if not code == '':
        switch(code)


topmenu = tk.Menu(root)
root.config(menu=topmenu)
cheats = tk.Menu(topmenu)
topmenu.add_cascade(menu=cheats, label='Cheats & Household Rules')
cheats.add_command(label='Enter Cheat Code', command=CheatCode)
cheats.add_separator()
InfiFlip = tk.BooleanVar()
InfiFlip.set(False)
BadTails = tk.BooleanVar()
BadTails.set(False)
sides10 = tk.BooleanVar()
sides10.set(False)
sides12 = tk.BooleanVar()
sides12.set(False)
sides20 = tk.BooleanVar()
sides20.set(False)


def PyCommands():
    global winner, loser, InfiFlip, BadTails, Teams, DiceSides, CoinSides
    pycode = simpledialog.askstring('', 'Enter a Python Command')
    exec(pycode)


cheats.add_checkbutton(label='InfiFlip', command=lambda: switch('InfiFlip'))
cheats.add_checkbutton(label='All6', command=lambda: switch('All6'))
cheats.add_checkbutton(label='BadTails', command=lambda: switch('BadTails'))
cheats.add_checkbutton(label='Sides10', command=lambda: switch('Sides10'))
cheats.add_checkbutton(label='Sides12', command=lambda: switch('Sides12'))
cheats.add_checkbutton(label='Sides20', command=lambda: switch('Sides20'))
cheats.add_separator()
cheats.add_command(label='Enter PyCommand', command=PyCommands)
cheats.add_separator()
cheats.add_command(label='InfiFlip Help', command=lambda: system(
    "open 'Cheat Help/infiflip.txt'"))
cheats.add_command(label='All6 Help', command=lambda: system(
    "open 'Cheat Help/all6.txt'"))
cheats.add_command(label='BadTails Help', command=lambda: system(
    "open 'Cheat Help/badtails.txt'"))
cheats.add_command(label='Sides10 Help', command=lambda: system(
    "open 'Cheat Help/Sides10.txt'"))
cheats.add_command(label='Sides12 Help', command=lambda: system(
    "open 'Cheat Help/Sides12.txt'"))
cheats.add_command(label='Sides20 Help', command=lambda: system(
    "open 'Cheat Help/Sides20.txt'"))


def flip():
    global status, index
    coin['state'] = states[0]
    die['state'] = states[1]
    outvar = str(choice(CoinSides))
    osmessage.showinfo('', 'You got a:\n' + outvar)
    if outvar == 'Tails':
        if BadTails.get() != True:
            status = option1
            if setting is True:
                osmessage.showinfo('', status)
            else:
                pass
        else:
            coin['state'] = states[1]
            die['state'] = states[0]
            if setting is True:
                osmessage.showinfo('', 'You lose your turn.')
            else:
                pass
            index = not index
            if index == True:
                win.title(Teams[0])
            if index == False:
                win.title(Teams[1])
            win.title()
    if outvar == 'Heads':
        status = option2
        if setting is True:
            osmessage.showinfo('', status)
        else:
            pass


def roll():
    global index, winner, loser
    die['state'] = states[0]
    coin['state'] = states[1]
    outvar = str(choice(DiceSides))
    osmessage.showinfo('', 'You rolled a:\n ' + outvar)
    if index == True:  # This means its Blue's turn
        win.title(Teams[1])
        if status == option1:
            BlueSwitchPoint = int(BluePoints.get()) + int(outvar)
            BluePoints.set(str(BlueSwitchPoint))
            if int(BluePoints.get()) >= winner:
                win.destroy()
                root.destroy()
                print('With a score of ' + BluePoints.get() + ' - ' +
                      RedPoints.get() + ',' + Teams[0] + ' is the winner!')
                osmessage.showinfo('Game Over', Teams[0] + ' Wins')
        if status == option2:
            BlueSwitchPoint = int(BluePoints.get()) + int(outvar)
            RedSwitchPoint = int(RedPoints.get()) - int(outvar)
            BluePoints.set(BlueSwitchPoint)
            RedPoints.set(str(RedSwitchPoint))
            if int(RedPoints.get()) < loser:
                RedPoints.set(str(loser))
            if int(RedPoints.get()) == loser or int(BluePoints.get()) >= winner:
                root.destroy()
                win.destroy()
                print('With a score of ' + BluePoints.get() + ' - ' +
                      RedPoints.get() + ',' + Teams[0] + ' is the winner!')
                osmessage.showinfo('Game Over', Teams[0] + ' Wins')

    if index == False:  # This means its Red's turn
        win.title(Teams[0])
        if status == option1:
            RedSwitchPoint = int(RedPoints.get()) + int(outvar)
            RedPoints.set(str(RedSwitchPoint))
            if int(RedPoints.get()) >= winner:
                win.destroy()
                root.destroy()
                print('With a score of ' + BluePoints.get() + ' - ' +
                      RedPoints.get() + ',' + Teams[1] + ' is the winner!')
                osmessage.showinfo('Game Over', Teams[1] + ' Wins')
        if status == option2:
            RedSwitchPoint = int(RedPoints.get()) + int(outvar)
            BlueSwitchPoint = int(BluePoints.get()) - int(outvar)
            RedPoints.set(str(RedSwitchPoint))
            BluePoints.set(str(BlueSwitchPoint))
            if int(BluePoints.get()) < loser:
                BluePoints.set(str(loser))
            if int(BluePoints.get()) == loser or int(RedPoints.get()) >= winner:
                root.destroy()
                win.destroy()
                print('With a score of ' + BluePoints.get() + ' - ' +
                      RedPoints.get() + ',' + Teams[1] + ' is the winner!')
                osmessage.showinfo('Game Over', Teams[1] + ' Wins')
    index = not index


coin = tk.Button(win, text='Flip Coin', command=flip, padx=50, pady=50)
die = tk.Button(win, text='Roll Die ', command=roll, padx=50, pady=50)
die['state'] = tk.DISABLED
coin.pack(), die.pack()
root.mainloop()
