
import pygame
from random import choice
pygame.init()
win = pygame.display.set_mode((800, 800))
pygame.display.set_caption('On the Hunt')
x = 0
y = 0
width = 20
height = 20
block_size = 20
spawns = [True, False]
spawned = []
spawnxy = []
SpawnedMovement = ['FORWARD', 'BACKWARD', 'LEFT', 'RIGHT', 'NOMEOVEMENT']


def Movement():
    global x, y
    keys = pygame.key.get_pressed()
    if keys[pygame.K_a]:
        x -= 21
    if keys[pygame.K_d]:
        x += 21
    if keys[pygame.K_w]:
        y -= 21
    if keys[pygame.K_s]:
        y += 21


def Spawn():
    inout = choice(spawns)
    if inout == True:
        spawned.append([0, 0, 20, 20])


def MaintainSpawned():
    for i in range(len(spawned)):
        movement = choice(SpawnedMovement)
        if movement == 'FORWARDS':
            spawned[i][1] = spawned[i][1] - 21
        if movement == 'BACKWARDS':
            spawned[i][1] = spawned[i][1] + 21
        if movement == 'LEFT':
            spawned[i][0] = spawned[i][0] - 21
        if movement == 'RIGHT':
            spawned[i][0\] = spawned[i][0] + 21
        pygame.draw.rect(win, (0, 255, 0),
                         (spawned[i][0], spawned[i][1], spawned[i][2], spawned[i][3]))


running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    win.fill((0, 0, 0))
    Movement()
    Spawn()
    # Start of Drawing Functions
    for BlockY in range(80):
        for BlockX in range(80):
            rect = pygame.Rect(BlockX*(block_size+1), BlockY *
                               (block_size+1), block_size, block_size)
            pygame.draw.rect(win, (255, 255, 255), rect)
            MaintainSpawned()
    pygame.draw.rect(win, (255, 0, 0), (x, y, width, height))
    # End of Drawing Functions
    pygame.display.update()
